package com.example.green.newapp.presenter;


public interface Presenter<V> {

    void attachedView(V view);

    void detachView();

    void onResume();

    void onItemSelected(int position);
}
