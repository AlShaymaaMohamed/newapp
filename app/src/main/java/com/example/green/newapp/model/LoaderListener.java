package com.example.green.newapp.model;

import java.util.ArrayList;


public interface LoaderListener {

    void onFinished(ArrayList<AthelteModel> pictureList);
}
