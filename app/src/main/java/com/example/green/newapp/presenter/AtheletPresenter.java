package com.example.green.newapp.presenter;

import com.example.green.newapp.model.AtheletInteractor;
import com.example.green.newapp.model.AthelteModel;
import com.example.green.newapp.model.LoaderListener;
import com.example.green.newapp.view.AtheletModelMVPview;

import java.util.ArrayList;

/**
 * Created by Green on 28/11/2017.
 */

public class AtheletPresenter implements Presenter<AtheletModelMVPview>,LoaderListener{

    private AtheletModelMVPview mvPview;
    private AtheletInteractor atheletInteractor;

    public AtheletPresenter(){
        atheletInteractor=new AtheletInteractor();
    }


    @Override
    public void attachedView(AtheletModelMVPview view) {
        if (view == null)
            throw new IllegalArgumentException("You can't set a null view");

        mvPview = view;
    }

    @Override public void detachView() {
        mvPview = null;
    }

    @Override public void onResume() {
        mvPview.showProgress();
        atheletInteractor.loadItems(this);
    }

    @Override public void onItemSelected(int position) {
        mvPview.showMessage(Integer.toString(position));

    }

    @Override public void onFinished(ArrayList<AthelteModel> pictureList) {
        mvPview.setItems(pictureList);
        mvPview.hideProgress();
    }
}
