package com.example.green.newapp.view;

import com.example.green.newapp.model.AthelteModel;

import java.util.ArrayList;

/**
 * Created by Green on 28/11/2017.
 */

public interface AtheletModelMVPview {
    void setItems(ArrayList<AthelteModel> pictureList);

    void showProgress();

    void hideProgress();

    void showMessage(String message);
}
