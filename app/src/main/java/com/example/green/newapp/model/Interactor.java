package com.example.green.newapp.model;


public interface Interactor {

    void loadItems(LoaderListener loaderListener);
}
